/*******************************************************************************
 *
 * File: OIJoystick.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "gsutilities/Advisory.h"
#include "rfutilities/OIJoystick.h"
#include "rfutilities/OIObserver.h"

#include "frc/Joystick.h"

#include <stdio.h>

using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a Joystick or Gamepad controller
 * 
 * @param	name	the name of this joystick, just for debugging clarity
 * @param	device_id	the joystick number  (1 thru 4)
 * 
 ******************************************************************************/
OIJoystick::OIJoystick(std::string name, int device_id)
	: OIDevice(name, 4, 12, 1)  // 4 analog, 12 digital, 1 pov.
{
	Advisory::pinfo("    creating joystick %d", device_id);
	stick = new Joystick(device_id);
}

/*******************************************************************************	
 *
 * Release any resources allocated by an instance of this class
 * 
 ******************************************************************************/
OIJoystick::~OIJoystick(void)
{
	if (stick != nullptr)
	{
		delete stick;
		stick = nullptr;
	}
}

/*******************************************************************************	
 *
 * override the oiddevice class to handle 1-based joystick values
 * 
 ******************************************************************************/
void OIJoystick::subscribeDigital(int chan, OIObserver *obs, int obs_data, 
    bool obs_invert)
{
    OIDevice::subscribeDigital(chan-1, std::bind(&OIObserver::setDigital, obs, obs_data, std::placeholders::_1), obs_invert);
}

/*******************************************************************************	
 *
 * Check all of the input channels, if any of them changed notify the 
 * observer of the change
 * 
 ******************************************************************************/
void OIJoystick::update(void)
{
	for (int i = 0; i < ANALOG_CHANNELS; i++)
	{
		updateAnalog(i, stick->GetRawAxis(i));
	}

	for (int i = 0; i < DIGITAL_CHANNELS; i++)
	{
		updateDigital(i, stick->GetRawButton(i+1)); // buttons are 1 based
	}

	for (int i = 0; i < INT_CHANNELS; i++)
	{
		updateInt(i, stick->GetPOV(i));
	}
}
