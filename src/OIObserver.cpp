/*******************************************************************************
 *
 * File: OIObserver.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 *  NOTE: This class is being phased out, it should not be used in new development
 * 
 ******************************************************************************/
#include "rfutilities/OIObserver.h"

/*******************************************************************************	
 *Constructor
 ******************************************************************************/
OIObserver::OIObserver(void)
{

}

/*******************************************************************************	
 *Destructor
 ******************************************************************************/
OIObserver::~OIObserver(void)
{

}

