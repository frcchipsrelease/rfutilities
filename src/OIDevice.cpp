/*******************************************************************************
 *
 * File: OIDevice.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/OIDevice.h"
#include "rfutilities/RobotUtil.h"

#include "gsutilities/Advisory.h"

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <limits>

/*******************************************************************************    
 * 
 * Create an instance of a an OI device
 * 
 ******************************************************************************/
OIDevice::OIDevice(std::string name, int a_channels, int d_channels, int i_channels, int s_channels)
    : device_name(name)
    , ANALOG_CHANNELS(a_channels)
    , DIGITAL_CHANNELS(d_channels)
    , INT_CHANNELS(i_channels)
    , STRING_CHANNELS(s_channels)
	, INVALID_INT(std::numeric_limits<int>::lowest())
	, INVALID_FLOAT(std::numeric_limits<float>::lowest())
	, INVALID_STRING("KKLSKJFIFakdjfkdj")
	, INVALID_BOOL(255)
{    
    Advisory::pinfo("OIDevice::OIDevice() - creating %s with %d analogs, %d digitals, %d ints, and %d strings",
            name.c_str(), a_channels, d_channels, i_channels, s_channels);

    // 
    // This dynamic allocation of arrays for holding information
    // allows the arrays to be used by subclasses with different numbers
    // of channels.
    //
    analog_val  = new float[ANALOG_CHANNELS];
    digital_val = new uint8_t[DIGITAL_CHANNELS];
    int_val     = new int[INT_CHANNELS];
    string_val  = new std::string[STRING_CHANNELS];

    analog_handles    = new std::vector<OIDeviceAnalogHandle>[ANALOG_CHANNELS];
    digital_handles   = new std::vector<OIDeviceDigitalHandle>[DIGITAL_CHANNELS];
    int_handles       = new std::vector<OIDeviceIntHandle>[INT_CHANNELS];
    string_handles    = new std::vector<OIDeviceStringHandle>[STRING_CHANNELS];

    // initialize all values
    //
    for (int i = 0; i < ANALOG_CHANNELS; i++)
    {
        analog_handles[i].clear();
        analog_val[i] = INVALID_FLOAT;
    }
    
    for (int i = 0; i < DIGITAL_CHANNELS; i++)
    {
        digital_handles[i].clear();
        digital_val[i] = INVALID_BOOL;
    }

    for (int i = 0; i < INT_CHANNELS; i++)
    {
        int_handles[i].clear();
        int_val[i] = INVALID_INT;
    }

    for (int i = 0; i < STRING_CHANNELS; i++)
    {
        string_handles[i].clear();
        string_val[i] = INVALID_STRING;
    }
}

/*******************************************************************************    
 *
 * Release any resources allocated by an instance of this class
 * 
 ******************************************************************************/
OIDevice::~OIDevice(void)
{
    if (analog_val != nullptr)
    {
        delete[] analog_val;
        analog_val = nullptr;
    }

    if (analog_handles != nullptr)
    {
        delete analog_handles;
        analog_handles = nullptr;
    }

    if (digital_val != nullptr)
    {
        delete[] digital_val;
        digital_val = nullptr;
    }

    if (digital_handles != nullptr)
    {
        delete digital_handles;
        digital_handles = nullptr;
    }

    if (int_val != nullptr)
    {
        delete[] int_val;
        int_val = nullptr;
    }

    if (int_handles != nullptr)
    {
        delete int_handles;
        int_handles = nullptr;
    }

    if (string_val != nullptr)
    {
        delete[] string_val;
        string_val = nullptr;
    }

    if (string_handles != nullptr)
    {
        delete string_handles;
        string_handles = nullptr;
    }
}

/*******************************************************************************    
 * 
 * Subscribe to receive an analog signal.
 * 
 * @param    chan       The device channel being subscribed to, note that
 *                      the channel is 0 based to match hardware labels.
 * 
 * @param    handle     A standard c++ function that can be called when the
 *                      value of a channel changes.
 *                 
 * @param    scale      The raw channel value will be multipled by this
 *                      scale before being passed to the observer, a value
 *                      of -1.0 can be used to invert the input
 * 
 * @param   deadband    Values that have a magnitude less than the deadband
 *                      will be reported as zero.
 *                 
 ******************************************************************************/
void OIDevice::subscribeAnalog(int channel, std::function<void(float)> handle, float scale, float deadband)
{
    if ((channel >= 0) && (channel < ANALOG_CHANNELS))
    {
        Advisory::pinfo("    subscribing to %s analog %d  scale = %5.2f  deadband=%5.2f",
            device_name.c_str(), channel, scale, deadband);

        OIDeviceAnalogHandle new_handle;
        new_handle.handle = handle;
        new_handle.scale = scale;
        new_handle.deadband = deadband;

        analog_handles[channel].push_back(new_handle);

        if(fabs(analog_val[channel] - INVALID_FLOAT) > 0.001)
        {
            if (fabs(analog_val[channel]) >= deadband )
            {
                handle(RobotUtil::deadbandJoy(analog_val[channel], deadband, 1.0) * scale);
            }
            else
            {
                handle(0.0);
            }
        }
    }
    else
    {
        Advisory::pcaution("   subscribe failed, asking for analog channel %d, only have %d", channel, ANALOG_CHANNELS);
    }
}

/*******************************************************************************    
 * 
 * Subscribe to receive an int signal.
 *
 * @param    chan        the device channel being subscribed to, note that
 *                         the channel is 0 based to match hardware labels.
 *
 * @param    handle     A standard c++ function that can be called when the
 *                      value of a channel changes.
 *                 
 * @param    scale    the raw channel value will be multipled by this
 *                         scale before being passed to the observer, a value
 *                         of -1.0 can be used to invert the input.
 *
 ******************************************************************************/
void OIDevice::subscribeInt(int channel, std::function<void(int)> handle, float scale)
{
    if ((channel >= 0) && (channel < INT_CHANNELS))
    {
		Advisory::pinfo("    subscribing to %s int %d  scale = %5.2f",
            device_name.c_str(), channel, scale);

        OIDeviceIntHandle new_handle;
        new_handle.handle = handle;
        new_handle.scale = scale;

        int_handles[channel].push_back(new_handle);

        if(int_val[channel] != INVALID_INT)
		{
            if (int_val[channel] >= 0)
			{
                handle((int)((int_val[channel] * scale) + 0.49999999));
			}
			else
			{
                handle((int)((int_val[channel] * scale) - 0.49999999));
			}
        }
    }
    else
    {
        Advisory::pcaution("   subscribe failed, asking for int channel %d, only have %d", channel, INT_CHANNELS);
    }
}

/*******************************************************************************
 *
 * Subscribe to receive an digital signal.
 * 
 * @param    chan           the device channel being subscribed to, note that
 *                          the channel is 0 based to match hardware labels.
 * 
 * @param    handle     A standard c++ function that can be called when the
 *                      value of a channel changes.
 *                 
 * @param    invert     if the state of this input channel is opposite of
 *                          what is desired, an invert of true can be used
 *                          to flip it.
 *                  
 ******************************************************************************/
void OIDevice::subscribeDigital(int channel, std::function<void(bool)> handle, bool invert)
{
    // in work
    if ((channel >= 0) && (channel < DIGITAL_CHANNELS))
    {
        Advisory::pinfo("    subscribing to %s digital %d (0-Based!) invert = %s",
            device_name.c_str(), channel, (invert?"true":"false"));

        OIDeviceDigitalHandle new_handle;
        new_handle.handle = handle;
        new_handle.invert = invert;

        digital_handles[channel].push_back(new_handle);
        if(digital_val[channel] != INVALID_BOOL)
        {
            handle(digital_val[channel] != invert);
        }
    }
    else
    {
        Advisory::pcaution("   subscribe failed, asking for digital channel %d, only have %d", channel, DIGITAL_CHANNELS);
    }
}

/*******************************************************************************
 *
 * Subscribe to receive an string signal.
 * 
 * @param    chan        the device channel being subscribed to, note that
 *                       the channel is 0 based to match hardware labels.
 * 
 * @param    handle     A standard c++ function that can be called when the
 *                      value of a channel changes.
 *                 
 ******************************************************************************/
void OIDevice::subscribeString(int chan, std::function<void(std::string)> handle)
{
    if ((chan >= 0) && (chan < STRING_CHANNELS))
    {
		Advisory::pinfo("    subscribing to %s string %d ",
			device_name.c_str(), chan);

		OIDeviceStringHandle new_handle;
		new_handle.handle = handle;

		string_handles[chan].push_back(new_handle);

		if(string_val[chan] != INVALID_STRING)
		{
			handle(string_val[chan]);
    	}
    }
}

/*******************************************************************************    
 * 
 * Update all the specified channel with a new value.  If the new value is 
 * different from the previous value, the observers will be notified.
 * 
 * @param    idx        the channel index (zero based) of the value being updated
 * 
 * @param    val        the new value of the specified channel
 *                 
 ******************************************************************************/
void OIDevice::updateAnalog(int idx, float val)
{
    if(fabs(val - INVALID_FLOAT) > 0.001)
	{
        if (fabs(analog_val[idx] - val) > 0.001) // if the value changed by enough
		{
            if (val > 1.0) {val = 1.0;}
			if (val < -1.0) {val = -1.0;}

            for (OIDeviceAnalogHandle obs : analog_handles[idx])
			{
                if (fabs(val) >= obs.deadband )
                {
                    obs.handle(val * obs.scale);
                }
                else
                {
                    if (fabs(analog_val[idx]) >= obs.deadband )
                    {
                        obs.handle(0.0);
                    }
                }
            }

			analog_val[idx] = val;
        }
    }
}

/*******************************************************************************    
 * 
 * Update all the specified channel with a new value.  If the new value is 
 * different from the previous value, the observers will be notified.
 * 
 * @param    idx        the channel index (zero based) of the value being updated
 * 
 * @param    val        the new value of the specified channel
 *                 
 ******************************************************************************/
void OIDevice::updateDigital(int idx, bool val)
{    
	if (digital_val[idx] != (uint8_t)val)
	{
        for(OIDeviceDigitalHandle obs : digital_handles[idx])
		{
            obs.handle(val != obs.invert);
		}

		digital_val[idx] = (uint8_t)val;
	}
}

/*******************************************************************************
 *
 * Update all the specified channel with a new value.  If the new value is
 * different from the previous value, the observers will be notified.
 *
 * @param    idx        the channel index (zero based) of the value being updated
 *
 * @param    val        the new value of the specified channel
 *
 ******************************************************************************/
void OIDevice::updateInt(int idx, int val)
{
	if (int_val[idx] != val)
	{
        for (OIDeviceIntHandle obs : int_handles[idx])
		{
			if (val >= 0)
			{
                obs.handle((int)((val * obs.scale) + 0.49999999)); // Truncation makes -1 go down to 0.
			}
			else
			{
                obs.handle((int)((val * obs.scale) - 0.49999999)); // Truncation makes -1 go down to 0.
			}
		}

		int_val[idx] = val;
	}
}

/*******************************************************************************    
 * 
 * Update all the specified channel with a new value.  If the new value is 
 * different from the previous value, the observers will be notified.
 * 
 * @param    idx        the channel index (zero based) of the value being updated
 * 
 * @param    val        the new value of the specified channel
 *                 
 ******************************************************************************/
void OIDevice::updateString(int idx, std::string val)
{    
	if(val != INVALID_STRING)
	{
		if (strcmp(string_val[idx].c_str(), val.c_str()) != 0)
		{
            for(OIDeviceStringHandle obs : string_handles[idx])
			{
                obs.handle(val);
			}

			string_val[idx] = val;
		}
	}
}
