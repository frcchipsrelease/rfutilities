/*******************************************************************************
 *
 * File: OIDriverStation.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/

#include <rfutilities/OIDriverStation.h>

#include "gsutilities/Advisory.h"

using namespace frc;

/*******************************************************************************
 *
 * Create an instance of a Driver Station. This class acts like a middle man
 * between the WPI DriverStation and OIController.
 *
 * @param	name	The name of the OIDriverStation.
 * @param	ds		The DriverStation OIDriverStation will communicate to.
 *
 ******************************************************************************/
OIDriverStation::OIDriverStation(std::string name)
	: OIDevice(name, NUM_ANALOG_CHANNELS, NUM_DIGITAL_CHANNELS, NUM_INT_CHANNELS, NUM_STRING_CHANNELS)
{
	Advisory::pinfo("Creating OI Driver Station %s", name.c_str());
}

/*******************************************************************************
 *
 * Release any resources allocated by an instance of this class.
 *
 ******************************************************************************/
OIDriverStation::~OIDriverStation()
{
}

/*******************************************************************************
 *
 * pull out the first byte from the 2017 message and set for game zone
 *
 ******************************************************************************/
OIDriverStation::AllianceColorDirection OIDriverStation::getGameZoneDirection(IntChannel channel)
{
    int index = 0;
    //Advisory::pinfo("getGameZoneDirection:: channel: %d message: -%s-", channel, m_driver_station.GetGameSpecificMessage().c_str() );

    // set the index into the string based on the channel selected
    if (channel == GAME_ZONE_NEAR)
    {
       index = 0;
    }
    else if (channel == GAME_ZONE_CENTER)
    {
        index = 1;
    }
    else if (channel == GAME_ZONE_FAR)
    {
        index = 2;
    }
    else
    {
        return ALLIANCE_COLOR_INVALID;
    }

    // check the length of the string for valid length
    if (DriverStation::GetGameSpecificMessage().length() < 3)
    {
        return ALLIANCE_COLOR_INVALID;
    }

    if (DriverStation::GetGameSpecificMessage()[index] == 'l' ||
        DriverStation::GetGameSpecificMessage()[index] == 'L')
    {
        return ALLIANCE_COLOR_LEFT;
    }
    else if (DriverStation::GetGameSpecificMessage()[index] == 'r' ||
        DriverStation::GetGameSpecificMessage()[index] == 'R')
    {
        return ALLIANCE_COLOR_RIGHT;
    }
    else
    {
        return ALLIANCE_COLOR_INVALID;
    }

}

/*******************************************************************************
 *
 * Check all of the input channels, if any of them changed notify the
 * observer of the change.
 *
 ******************************************************************************/
void OIDriverStation::update(void)
{
	updateAnalog(MATCH_TIME, (double)DriverStation::GetMatchTime());
	updateAnalog(BATTERY_VOLTAGE, DriverStation::GetBatteryVoltage());

	updateDigital(IS_ENABLED, DriverStation::IsEnabled());
	updateDigital(IS_AUTON, DriverStation::IsAutonomous());
	updateDigital(IS_TEST, DriverStation::IsTest());
	updateDigital(IS_DS_ATTACHED, DriverStation::IsDSAttached());
	updateDigital(IS_FMS_ATTACHED, DriverStation::IsEnabled());
	updateDigital(IS_SYS_ACTIVE, RobotController::IsSysActive());
	updateDigital(IS_SYS_BROWNED_OUT, RobotController::IsBrownedOut());
//	updateDigital(IS_SYS_ACTIVE, DriverStation::IsSysActive());
//	updateDigital(IS_SYS_BROWNED_OUT, DriverStation::IsBrownedOut());

// TODO 2024 change. Need to scour docs to figure out the change
//	updateInt(ALLIANCE, DriverStation::GetAlliance());
//	updateInt(LOCATION, DriverStation::GetLocation());
	updateInt(GAME_ZONE_NEAR,   getGameZoneDirection(GAME_ZONE_NEAR));
	updateInt(GAME_ZONE_CENTER, getGameZoneDirection(GAME_ZONE_CENTER));
	updateInt(GAME_ZONE_FAR,    getGameZoneDirection(GAME_ZONE_FAR));

    updateString(EVENT_NAME,  DriverStation::GetEventName());
    updateString(GAME_MESSAGE, DriverStation::GetGameSpecificMessage());
}
