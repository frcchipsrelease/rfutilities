/*******************************************************************************
 *
 * File: OIController.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIDevice.h"
#include "rfutilities/OIJoystick.h"

#include "gsutilities/tinyxml2.h"

#include <functional>
#include <iostream>
#include <map>

/*******************************************************************************
 *
 * The OI Controller holds a reference to each of the OI Devices used by this
 * robot so the components of the robot can register to receive events from 
 * a single interface.
 * 
 ******************************************************************************/
class OIController
{
	public:
		static void configure(tinyxml2::XMLElement *xml);

		static void addDevice(std::string device, OIDevice *oi_device);
		
		/**
		 * The software is being updated to use the callback handles instead of 
		 * the observer interface, for both can be used but we are working 
		 * toward function/method callbacks so we can eliminate the OIObserver.
		 */
		static void subscribeAnalog(tinyxml2::XMLElement *xml, std::function<void(float)> handle);	
		static void subscribeAnalog(std::string device, int chan, std::function<void(float)> handle, float obs_scale, float obs_deadband);
		static void subscribeAnalog(tinyxml2::XMLElement *xml, OIObserver *obs, int obs_data);	
		static void subscribeAnalog(std::string device, int chan, OIObserver *obs, int obs_data, float obs_scale, float obs_deadband);
		
		static void subscribeInt(tinyxml2::XMLElement *xml, std::function<void(int)> handle);
		static void subscribeInt(std::string device, int chan, std::function<void(int)> handle, float obs_scale = 1.0f);
		static void subscribeInt(tinyxml2::XMLElement *xml, OIObserver *obs, int obs_data);
		static void subscribeInt(std::string device, int chan, OIObserver *obs, int obs_data, float obs_scale = 1.0f);

		static void subscribeDigital(tinyxml2::XMLElement *xml, std::function<void(bool)> handle);
		static void subscribeDigital(std::string device, int chan, std::function<void(bool)> handle, bool obs_invert);
		static void subscribeDigital(tinyxml2::XMLElement *xml, OIObserver *obs, int obs_data);	
		static void subscribeDigital(std::string device, int chan, OIObserver *obs, int obs_data, bool obs_invert);

		static void subscribeString(tinyxml2::XMLElement *xml, std::function<void(std::string)> handle);
		static void subscribeString(std::string device, int chan, std::function<void(std::string)> handle);
		static void subscribeString(tinyxml2::XMLElement *xml, OIObserver *obs, int obs_data);
		static void subscribeString(std::string device, int chan, OIObserver *obs, int obs_data);
		
		// For completeness, there should be an unsubscribe but
		// we don't use it so there's not
		// static void unsubscribeAll(OIObserver *obs);
		
		static void update(void);

	private:		
		static std::map<std::string, OIDevice *> device_map;
};

