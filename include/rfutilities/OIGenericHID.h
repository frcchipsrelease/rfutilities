/*******************************************************************************
 *
 * File: OIJoystick.h
 * 
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIDevice.h"

#include "frc/Joystick.h"


/*******************************************************************************
 *
 * This OI Device creates an event dispatcher for monitoring the buttons and
 * dials on a HID.
 *
 ******************************************************************************/
class OIGenericHID : public OIDevice
{
	public:
		OIGenericHID(std::string name, int analogs, int digitals, int povs, int device_id);
		~OIGenericHID(void);

		void update(void);

	protected:
        frc::Joystick *stick;
};
