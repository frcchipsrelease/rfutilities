/*******************************************************************************
 *
 * File: OIDriverStation.h
 *
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIDevice.h"

#include "frc/DriverStation.h"
#include "frc/RobotController.h"

class OIDriverStation : public OIDevice
{
	public:
		enum AnalogChannel
		{
			MATCH_TIME = 0, BATTERY_VOLTAGE, NUM_ANALOG_CHANNELS
		};

		enum DigitalChannel
		{
			IS_ENABLED = 0, IS_AUTON, IS_TELEOP, IS_TEST, IS_DS_ATTACHED,
			IS_FMS_ATTACHED, IS_SYS_ACTIVE, IS_SYS_BROWNED_OUT,
			NUM_DIGITAL_CHANNELS
		};

		enum IntChannel
		{
			ALLIANCE = 0, LOCATION, GAME_ZONE_NEAR, GAME_ZONE_CENTER, GAME_ZONE_FAR, NUM_INT_CHANNELS
		};

		enum StringChannel
		{
				EVENT_NAME = 0, GAME_MESSAGE, NUM_STRING_CHANNELS
		};

		enum AllianceColorDirection
		{
			ALLIANCE_COLOR_LEFT = 0, ALLIANCE_COLOR_RIGHT, ALLIANCE_COLOR_INVALID, NUM_ALLIANCE_COLOR_DIRECTIONS
		};

        OIDriverStation(std::string name);

		virtual ~OIDriverStation();

		void update(void);
		
        AllianceColorDirection getGameZoneDirection(IntChannel);

	private:

};
