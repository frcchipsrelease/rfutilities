/*******************************************************************************
 *
 * File: OIJoystick.h
 * 
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIDevice.h"

#include "frc/joystick.h"

/*******************************************************************************
 *
 * This OI Device creates an event dispatcher for monitoring the buttons and
 * dials on a joystick or gamepad.
 *
 ******************************************************************************/
class OIJoystick : public OIDevice
{
	public:
		OIJoystick(std::string name, int device_id);
		~OIJoystick(void);

		void update(void);

		void subscribeDigital(int chan, OIObserver *obs, int obs_data, bool obs_invert);

	protected:
        frc::Joystick *stick;
};
