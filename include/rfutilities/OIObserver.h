/*******************************************************************************
 *
 * File: OIObserver.h
 * 
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <string>

/*******************************************************************************
 *
 * This OI Observer is a pure virtual class that acts as an interface for
 * any class that needs to be notified when the one or more of the inputs
 * from the Operator interface changes.
 *
 * NOTE: This class is being phased out, it should not be used in 
 *       new development.
 * 
 ******************************************************************************/
class OIObserver
{
	public:
		enum PovDirection
		{
			POV_NORTH = 0,
			POV_NORTHEAST = 45,
			POV_EAST = 90,
			POV_SOUTHEAST = 135,
			POV_SOUTH = 180,
			POV_SOUTHWEST = 225,
			POV_WEST = 270,
			POV_NORTHWEST = 315
		};

		OIObserver();
		virtual ~OIObserver();
		virtual void setAnalog(int id, float val) {}
		virtual void setInt(int id, int val) {}
		virtual void setDigital(int id, bool val) {}
		virtual void setString(int id, std::string val) {}
};
