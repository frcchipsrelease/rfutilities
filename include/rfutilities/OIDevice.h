/*******************************************************************************
 *
 * File: OIDevice.h
 * 
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfutilities/OIObserver.h"

#include <string>
#include <vector>
#include <functional>


/*******************************************************************************
 *
 *  @class   OIDeviceAnalogHandle - A small internal class for holding 
 * 			information about the analog callback methods
 * 
 ******************************************************************************/
class OIDeviceAnalogHandle
{
    public:
        std::function<void(float)> handle;
        float scale;
        float deadband;
};

/*******************************************************************************
 *
 *  @class   OIDeviceIntHandle - A small internal class for holding information 
 * 			about the integer callback methods
 * 
 ******************************************************************************/
class OIDeviceIntHandle
{
    public:
        std::function<void(int)> handle;
        float scale;
};

/*******************************************************************************
 *
 *  @class   OIDeviceDigitalHandle - A small internal class for holding 
 * 			information about the boolean callback methods
 * 
 ******************************************************************************/
class OIDeviceDigitalHandle
{
    public:
        std::function<void(bool)> handle;
        bool  invert;
};

/*******************************************************************************
 *
 *  @class   OIDeviceStringHandle - A small internal class for holding 
 * 			information about the string callback methods
 * 
 ******************************************************************************/
class OIDeviceStringHandle
{
    public:
        std::function<void(std::string)> handle;
};


/*******************************************************************************
 *
 * This base class is the base for all IO Devices, such as OIDriverStation 
 * and OIJoystick
 *
 ******************************************************************************/
class OIDevice
{
	public:
		enum PovDirection
		{
			POV_NORTH = 0,
			POV_NORTHEAST = 45,
			POV_EAST = 90,
			POV_SOUTHEAST = 135,
			POV_SOUTH = 180,
			POV_SOUTHWEST = 225,
			POV_WEST = 270,
			POV_NORTHWEST = 315
		};

		OIDevice(std::string name, int analog_channels, int digital_channels, 
			int int_channels = 0, int string_channels = 0);

		virtual ~OIDevice();
		
		virtual void subscribeAnalog(int chan, std::function<void(float)> handle, 
			float obs_scale = 1.0, float obs_deadband = 0.0);

		virtual void subscribeInt(int chan, std::function<void(int)> handle, 
			float obs_scale = 1.0f);

		virtual void subscribeDigital(int chan, std::function<void(bool)> handle, 
			bool invert = false);
			
		virtual void subscribeString(int chan, std::function<void(std::string)> handle);
		
		// For completeness there should an unsubscribe, but we won't use it so it's not there
		// virtual void unsubscribeAll(OIObserver *obs);
		
		virtual void update() = 0;
		
	protected:
		virtual void updateAnalog(int idx, float val);
		virtual void updateDigital(int idx, bool val);
		virtual void updateInt(int idx, int val);
		virtual void updateString(int idx, std::string val);
		
		const std::string device_name;

		/** 
		 * These constants are initialized during cunstruction based 
		 * on informatino passed to the constructor
		 */
		const int ANALOG_CHANNELS;
		const int DIGITAL_CHANNELS;
		const int INT_CHANNELS;
		const int STRING_CHANNELS;

		/**
		 * These constants define invalid values for channel data
		 * just so we have a starting point for all data 
		 */
		const int         INVALID_INT;
		const float       INVALID_FLOAT;
		const std::string INVALID_STRING;
		const uint8_t     INVALID_BOOL;

	    /**
		 * These arrays hold the current value for each channel of the device.
		 * They are all initialized to the INVALID_<type> value when an instance
		 * of this class is created. The arrays are dynamically sized based on
		 * information passed to the constructor.
		 */
		float       *analog_val;
		uint8_t     *digital_val;
		int         *int_val;
		std::string *string_val;

		/**
		 * These vectors hold information about each subscription to a
		 * device channel. There is a vector created for each channel of
		 * the device in the constructor based on information passed to 
		 * the constructor.
		 */
        std::vector<OIDeviceAnalogHandle>  *analog_handles;
        std::vector<OIDeviceDigitalHandle> *digital_handles;
        std::vector<OIDeviceIntHandle>     *int_handles;
        std::vector<OIDeviceStringHandle>  *string_handles;
};
