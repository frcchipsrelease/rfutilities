/*******************************************************************************
 *
 * File: DataLogger.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <stdio.h>
#include <stdarg.h>
#include <string>
#include <gsutilities/SegmentedFile.h>

/*******************************************************************************
 *
 * This class contains a generic interface for creating and managing log
 * files on the robot.
 *
 ******************************************************************************/
class DataLogger
{
	public:
        DataLogger(std::string path, std::string name, std::string type, uint32_t max_count, bool enabled=true);
		~DataLogger(void);

		void openSegment(void);

        void setEnabled(bool enabled);
		bool isEnabled(void);
		int32_t getCurrentIndex(void);

		void log(const char* fmt, ...);
		void flush();
		void close();
		
	private:
		bool m_enabled;
		gsu::SegmentedFile m_segmented_file;

		FILE *m_file;
};
